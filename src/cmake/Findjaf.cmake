foreach(comp IN LISTS jaf_FIND_COMPONENTS)
    find_package(jaf-${comp} ${jaf_FIND_VERSION} CONFIG QUIET)

    if(jaf-${comp}_FOUND)
        set(jaf_${comp}_FOUND "${jaf-${comp}_FOUND}")
    endif()
endforeach()

set(JAF_LOCATION "${CMAKE_CURRENT_LIST_DIR}")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
    jaf
    REQUIRED_VARS JAF_LOCATION
    HANDLE_VERSION_RANGE
    HANDLE_COMPONENTS
)
