from conans import ConanFile, CMake

class JafCorePort(ConanFile):
    name = "jaf_core"
    version = "0.0.1"
    description = "core library for jaf framework"
    url = "https://gitlab.com/just-a-framework/libraries/core"
    generators = "cmake"
    exports_sources = "src*"
    build_requires = "cmake/3.21.1"

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CONAN_BUILD"] = "ON"
        cmake.configure(source_folder="src")
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()
    
    def package_info(self):
        self.cpp_info.builddirs = ["lib/jaf/cmake"]
