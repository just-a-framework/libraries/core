# just-a-framework :: core

The core library which allows the usage of:
```cmake
find_package(jaf COMPONENTS <components>)
```